#!/usr/bin/python3
import json
import os
from os.path import expanduser
import configparser


class Config:
    def __init__(self):
        home = expanduser("~")
        config_file = "{}/.config/tuxbuild/config.ini".format(home)
        self.config = configparser.ConfigParser()
        self.config.read(config_file)
        self.tuxbuild_env = os.getenv("TUXBUILD_ENV", "default")
        default_api_url = (
            "https://api.tuxbuild.com/v1"  # Use production v1 if no url is specified
        )
        self.kbapi_url = (
            self.config[self.tuxbuild_env].get("api_url", default_api_url).rstrip("/")
        )

    def get_auth_token(self):
        return self.config[self.tuxbuild_env]["token"]

    def get_kbapi_url(self):
        return self.kbapi_url

    def get_tuxbuild_env(self):
        return self.tuxbuild_env
