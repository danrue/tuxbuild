#!/usr/bin/python3
import json
import random
import re
import requests
import time

status_attempts = int(
    60 * 120 / 7
)  # Total attempts to check for status. 7 is average sleep time
build_submit_attempts = 6


def post_request(kbapi_url, token, request):
    url = kbapi_url + "/build"
    headers = {"Content-type": "application/json", "Authorization": "{}".format(token)}
    response = requests.post(url, data=json.dumps(request), headers=headers)
    if response.status_code == 200:
        return json.loads(response.text)
    else:
        response.raise_for_status()  # Some unexpected status that's not caught


class Build:
    def __init__(
        self, git_repo, git_ref, target_arch, defconfig, toolchain, token, kbapi_url
    ):
        self.git_repo = git_repo
        self.git_ref = git_ref
        self.target_arch = target_arch
        self.defconfig = defconfig
        self.toolchain = toolchain
        self.build_data = None
        self.build_key = None
        self.token = token
        self.kbapi_url = kbapi_url

        self.verify_build_parameters()

    def __str__(self):
        return "{} {} with {} @ {}".format(
            self.target_arch, self.defconfig, self.toolchain, self.build_data
        )

    @staticmethod
    def is_supported_git_url(url):
        """ Check that the git url provided is valid (namely, that it's not an ssh url) """
        return re.match(r"^(git://|https?://).*$", url) is not None

    def generate_build_request(self):
        """ return a build data in a python dict"""
        build_entry = {}
        build_entry["git_repo"] = self.git_repo
        build_entry["git_ref"] = self.git_ref
        build_entry["target_arch"] = self.target_arch
        build_entry["toolchain"] = self.toolchain
        build_entry["defconfig"] = self.defconfig
        return build_entry

    def verify_build_parameters(self):
        """ Pre-check the build arguments """
        assert self.is_supported_git_url(
            self.git_repo
        ), "git url must be in the form of git:// or http:// or https://"
        assert self.git_ref is not None
        assert self.target_arch is not None
        assert self.defconfig is not None
        assert self.token is not None

    def build(self):
        """ Submit the build request """
        data = []
        data.append(self.generate_build_request())
        json_data = post_request(self.kbapi_url, self.token, data)
        self.build_data = json_data[0]["download_url"]
        self.build_key = json_data[0]["build_key"]

    def get_status(self):
        """ Fetches the build status and updates the values inside the build object"""
        headers = {
            "Content-type": "application/json",
            "Authorization": "{}".format(self.token),
        }
        url = self.kbapi_url + "/status/" + self.build_key
        for i in range(build_submit_attempts):
            r = requests.get(url, headers=headers)
            if r.status_code == 200:
                self.status = json.loads(r.text)
                break
            elif r.status_code == 504:  # 504 Server Error: Gateway Timeout for url
                pass
            else:
                r.raise_for_status()  # Some unexpected status that's not caught

    def wait_for_status(self, status):
        """
            Wait until the given status changes

            For example, if status is 'queued', wait_for_status
            will return once the status is no longer 'queued'

            Will timeout after status_attempts
        """
        for i in range(status_attempts):
            self.get_status()
            if self.status["tuxbuild_status"] != status:
                break
            time.sleep(random.randrange(15))

    def _get_field(self, field):
        """ Retrieve an individual field from status.json """
        self.get_status()
        return self.status.get(field, None)

    @property
    def warnings_count(self):
        """ Get the warnings_count for the build """
        return int(self._get_field("warnings_count"))

    @property
    def errors_count(self):
        """ Get the errors_count for the build """
        return int(self._get_field("errors_count"))

    @property
    def tuxbuild_status(self):
        """ Get the tuxbuild_status for the build """
        return self._get_field("tuxbuild_status")

    @property
    def build_status(self):
        """ Get the build_status for the build """
        return self._get_field("build_status")


class BuildSet:
    def __init__(self, build_objects, token, kbapi_url):
        self.build_objects = build_objects
        self.token = token
        self.kbapi_url = kbapi_url

    def build(self):
        """ Submit the build request """

        build_objects_list = self.build_objects[:]
        data = []
        for build_object in self.build_objects:
            data.append(build_object.generate_build_request())
        json_data = post_request(self.kbapi_url, self.token, data)
        for build_config in json_data:
            for build_object in build_objects_list:
                if (
                    build_config.items()
                    >= build_object.generate_build_request().items()
                ):
                    build_object.build_data = build_config["download_url"]
                    build_object.build_key = build_config["build_key"]
                    # We may have build set with same parameters multiple times
                    # hence remove the build from build_objects list after mapping
                    build_objects_list.remove(build_object)
                    break
